package swear;

import java.io.*;
import java.util.*;

/**
* main method where we run our program
* @author	Oleksandr Demydenko
*/
public class MainRuner {
	public static void main(String[] args) {
			switch(args[0]) {
				case "string": {
					StringLoader loaderLang = new StringLoader("lang.properties");
					StringLoader loaderQuestion = new StringLoader("questions.properties");
					Swear swear = new Swear(loaderLang, loaderQuestion);
					swear.run();
					break;
				}
				case "properties": {
					PropertiesLoader loaderLang = new PropertiesLoader("lang.properties");
					PropertiesLoader loaderQuestion = new PropertiesLoader("questions.properties");
					Swear swear = new Swear(loaderLang, loaderQuestion);
					swear.run();
					break;
				}
				default:
					System.out.println("Not right argument");
					break;
			}
	}
}