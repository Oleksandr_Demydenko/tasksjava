package swear;

import java.io.*;
import java.util.*;

/**
* The StringLoader is a class that load keys and values from properties file
* in right order
* @author	Oleksandr Demydenko
*/
public class StringLoader extends Loader {

	/**
	* @param pathToPropertiesFile - the location of the properties file
	*/
	public StringLoader(String pathToPropertiesFile) {
		
		try {
			File file = new File(pathToPropertiesFile);
			Scanner sc = new Scanner(file);
			String[] keysAndValues;
			
			while (sc.hasNextLine()) {
				keysAndValues = sc.nextLine().split(" = ");
				map.put(keysAndValues[0], keysAndValues[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}