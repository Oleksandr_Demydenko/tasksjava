package swear;

import java.io.*;
import java.util.*;

/**
* The Loader is a abstract class.
* @author	Oleksandr Demydenko
*/
public abstract class Loader {
	
	protected Map<String, String> map = new LinkedHashMap();

	/**
	* @return string array of languages keys
	*/
	public String[] getKeys() {
		Set<String> setKeys = map.keySet();
		return setKeys.toArray(new String[setKeys.size()]);
	}

	/**
	* @param userAnswer - the userAnswer is a number which user input
	* @return string of value
	*/
	public String getValue(int userAnswer) {
		String value = map.get(getKeys()[userAnswer]);
		return value;
	}

	/**
	* @param key - the key from properties
	* @return string of value
	*/
	public String getValue(String key) {
		String value = map.get(key);
		return value;
	}
}