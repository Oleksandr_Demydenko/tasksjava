package swear;

import java.io.*;
import java.util.*;

public class PropertiesLoader extends Loader {

	/**
	* @param pathToPropertiesFile - the location of the properties file
	*/
	public PropertiesLoader(String pathToPropertiesFile) {
		
		try {
			Properties properties = new Properties();
			FileInputStream inputStream = new FileInputStream (pathToPropertiesFile);
			properties.load(inputStream);
			Set<String> keys = properties.stringPropertyNames();
			
			for (String keyValue: keys) {
				map.put(keyValue, properties.getProperty(keyValue));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}