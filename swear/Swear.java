package swear;

import java.util.*;

/**
* The Swear class implements an application that
* simply displays string to the standard output depending on user answer.
*
* @author	Oleksandr Demydenko
*/
public class Swear {
	
	private Loader loaderLang = null;
	private Loader loaderQuestion = null;
	
	public Swear(Loader loaderLang, Loader loaderQuestion) {
		this.loaderLang = loaderLang;
		this.loaderQuestion = loaderQuestion;
	}

	/** create string with question and languages options */
	private void askQuestion() {
		String languageOptions = "";
		String[] languagesKeys = loaderLang.getKeys();
		
		for (int i = 0; i < languagesKeys.length; i++) {
			languageOptions = languageOptions + i + "-" + languagesKeys[i] + " ";
		}
		System.out.printf( loaderQuestion.getValue("choose.language") + " %s",languageOptions + ": ");
	}
	
	/** 
	* read user's answer and write it into variable userAnswer
	* @return integer variable of user input
	*/
	private int readAnswer() {
		Scanner scaner = new Scanner(System.in);
		
		while (!scaner.hasNextInt()) {
			System.out.println(loaderQuestion.getValue("error.message"));
			scaner.next();
		}
		return scaner.nextInt();
	}

	/**
	* show right value
	* @param value - string of value from properties
	*/
	private void showResult(String value) {
		System.out.println(value);
	}

	public void run() {
		askQuestion();
		int userAnswer = readAnswer();
		showResult(loaderLang.getValue(userAnswer));
	}
}