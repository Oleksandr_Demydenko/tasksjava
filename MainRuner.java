import java.io.*;
import java.util.*;
import loader.*;
import swear.*;

public class MainRuner {
	public static void main(String[] args) {
		Loader loader = new Loader("lang");
		Swear swear = new Swear(loader);
		swear.askQuestion();
		int userAnswer = swear.readAnswer();
		swear.showResult(loader.getValue(userAnswer));
	}
}