package tasksjava;

import java.io.*;
import java.util.*;
/**
* The Swear class implements an application that
* simply displays string to the standard output depending on user answer.
*
* @author	Oleksandr Demydenko
*/
public class Swear {
	
	private static Properties languageProperties = new Properties();
	private static Map<String, String> languageMap = new LinkedHashMap();

	/* load data from properties */
	static void loadData() {
		try{
			FileInputStream inputStream = new FileInputStream ("lang");
			languageProperties.load(inputStream);
			Set<String> languagesKeys = languageProperties.stringPropertyNames();
			for (String language: languagesKeys) {
				languageMap.put(language, languageProperties.getProperty(language));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* return array of languages keys */
	static String[] getLanguagesKeys() {
		Set<String> languageSetKeys = languageMap.keySet();
		return languageSetKeys.toArray(new String[languageSetKeys.size()]);
	}

	/* create string with question and languages options */
	static void askQuestion() {
		String languageOptions = "";
		for (int i = 0; i<languageMap.size(); i++) {
		languageOptions = languageOptions + i + "-" + getLanguagesKeys()[i] + " ";
		}
		System.out.print("In what language should the data be displayed? " + languageOptions + ": ");
	}
	
	/* read user's answer and show right value*/
	static void readAnswerAndShowResult() {
		Scanner scaner = new Scanner(System.in);
		while (!scaner.hasNextInt()) {
			System.out.println("Incorrect input, try again");
			scaner.next();
		}
		int userAnswer = scaner.nextInt();
		System.out.println (languageMap.get(getLanguagesKeys()[userAnswer]));
	}
	
	public static void main(String[] args) {
		loadData();
		askQuestion();
		readAnswerAndShowResult();
	}
}

